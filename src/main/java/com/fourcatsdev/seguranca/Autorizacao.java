package com.fourcatsdev.seguranca;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Autorizacao {
	// aula 19
	private Map<String, List<String>> auth = new HashMap<String, List<String>>();
	
	// aula 19
	public Autorizacao() {
		auth.put("ADMIN", Arrays.asList(new String[] {"/auth/user", "/auth/biblio", "/auth/admin"}));
		auth.put("BIBLIO",Arrays.asList(new String[] {"/auth/user", "/auth/biblio"}));
		auth.put("USER", Arrays.asList(new String[] {"/auth/user"}));
	}
	
	// aula 19
	public boolean temAutorizacao(DetalheUsuario detalheUsuario, String path) {
		boolean autorizado = false;
		for(String papelUsuario: detalheUsuario.getPapeis()) {
			List<String> confPapel = auth.get(papelUsuario);
			for (String url : confPapel) {
				if (path.contains(url)) {
					autorizado = true;
					break;
				}
			}
			if (autorizado) break;
		}		
		return autorizado;
	}
	
	// aula 18
	public String indexPerfil(DetalheUsuario detalheUsuario) {
		String redirectURL = "";
		if (verificarPerfil(detalheUsuario, "ADMIN")) {
			redirectURL = "auth/admin/admin-index.jsp";
		} else if (verificarPerfil(detalheUsuario, "BIBLIO")) {
			redirectURL = "auth/biblio/biblio-index.jsp";
		} else if (verificarPerfil(detalheUsuario, "USER")) {
			redirectURL = "auth/user/user-index.jsp";
		}
		return redirectURL;
	}	
	// aula 18
	private boolean verificarPerfil(DetalheUsuario detalheUsuario, String papel) {
		boolean temPerfil = detalheUsuario.getPapeis().contains(papel);
		return temPerfil;
	}
}
